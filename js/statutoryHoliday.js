/**

 @Name : 法定节假日配置
 @Author: mtr
 @since 2021-08-12
 字段描述：
     // 日期
     date: '2021-10-01',
     // 节假日名称
     holidayName: '国庆节',
     // 是否节假日当天
     isHolidayDay: true,
     // 是否补班
     isWork: false,
  数据来源：中国政府网_中央人民政府门户网站
  2021：http://www.gov.cn/zhengce/content/2020-11/25/content_5564127.htm
  2022：http://www.gov.cn/zhengce/content/2021-10/25/content_5644835.htm
 */
const TD_STATUTORY_HOLIDAY = [
    /** -------------------------------- 2021年 -------------------------------- */
    // 2021年元旦 - 休
    {date: '2021-01-01', holidayName: '元旦', isHolidayDay: true, isWork: false},
    {date: '2021-01-02', holidayName: '元旦', isHolidayDay: false, isWork: false},
    {date: '2021-01-03', holidayName: '元旦', isHolidayDay: false, isWork: false},
    // 2021年春节 - 休
    {date: '2021-02-11', holidayName: '除夕', isHolidayDay: true, isWork: false},
    {date: '2021-02-12', holidayName: '春节', isHolidayDay: true, isWork: false},
    {date: '2021-02-13', holidayName: '初二', isHolidayDay: true, isWork: false},
    {date: '2021-02-14', holidayName: '初三', isHolidayDay: true, isWork: false},
    {date: '2021-02-15', holidayName: '初四', isHolidayDay: true, isWork: false},
    {date: '2021-02-16', holidayName: '初五', isHolidayDay: true, isWork: false},
    {date: '2021-02-17', holidayName: '初六', isHolidayDay: true, isWork: false},
    // 2021年春节 - 班
    {date: '2021-02-07', holidayName: '春节', isHolidayDay: false, isWork: true},
    {date: '2021-02-20', holidayName: '春节', isHolidayDay: false, isWork: true},
    // 2021年清明节 - 休
    {date: '2021-04-03', holidayName: '寒食节', isHolidayDay: false, isWork: false},
    {date: '2021-04-04', holidayName: '清明节', isHolidayDay: true, isWork: false},
    {date: '2021-04-05', holidayName: '清明节', isHolidayDay: false, isWork: false},
    // 2021年劳动节 - 休
    {date: '2021-05-01', holidayName: '劳动节', isHolidayDay: true, isWork: false},
    {date: '2021-05-02', holidayName: '劳动节', isHolidayDay: false, isWork: false},
    {date: '2021-05-03', holidayName: '劳动节', isHolidayDay: false, isWork: false},
    {date: '2021-05-04', holidayName: '劳动节', isHolidayDay: false, isWork: false},
    {date: '2021-05-05', holidayName: '劳动节', isHolidayDay: false, isWork: false},
    // 2021年劳动节 - 班
    {date: '2021-04-25', holidayName: '劳动节', isHolidayDay: false, isWork: true},
    {date: '2021-05-08', holidayName: '劳动节', isHolidayDay: false, isWork: true},
    // 2021年端午节 - 休
    {date: '2021-06-12', holidayName: '端午节', isHolidayDay: false, isWork: false},
    {date: '2021-06-13', holidayName: '端午节', isHolidayDay: false, isWork: false},
    {date: '2021-06-14', holidayName: '端午节', isHolidayDay: true, isWork: false},
    // 2021年中秋节 - 休
    {date: '2021-09-19', holidayName: '中秋节', isHolidayDay: false, isWork: false},
    {date: '2021-09-20', holidayName: '中秋节', isHolidayDay: false, isWork: false},
    {date: '2021-09-21', holidayName: '中秋节', isHolidayDay: true, isWork: false},
    // 2021年中秋节 - 班
    {date: '2021-09-18', holidayName: '中秋节', isHolidayDay: false, isWork: true},
    // 2021年国庆 - 休
    {date: '2021-10-01', holidayName: '国庆节', isHolidayDay: true, isWork: false},
    {date: '2021-10-02', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    {date: '2021-10-03', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    {date: '2021-10-04', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    {date: '2021-10-05', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    {date: '2021-10-06', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    {date: '2021-10-07', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    // 2021年国庆 - 班
    {date: '2021-09-26', holidayName: '国庆节', isHolidayDay: false, isWork: true},
    {date: '2021-10-09', holidayName: '国庆节', isHolidayDay: false, isWork: true},

    /** -------------------------------- 2022年 -------------------------------- */
    // 2022年元旦 - 休
    {date: '2022-01-01', holidayName: '元旦', isHolidayDay: true, isWork: false},
    {date: '2022-01-02', holidayName: '元旦', isHolidayDay: false, isWork: false},
    {date: '2022-01-03', holidayName: '元旦', isHolidayDay: false, isWork: false},
    // 2022年春节 - 休
    {date: '2022-01-31', holidayName: '除夕', isHolidayDay: true, isWork: false},
    {date: '2022-02-01', holidayName: '春节', isHolidayDay: true, isWork: false},
    {date: '2022-02-02', holidayName: '初二', isHolidayDay: true, isWork: false},
    {date: '2022-02-03', holidayName: '初三', isHolidayDay: true, isWork: false},
    {date: '2022-02-04', holidayName: '初四', isHolidayDay: true, isWork: false},
    {date: '2022-02-05', holidayName: '初五', isHolidayDay: true, isWork: false},
    {date: '2022-02-06', holidayName: '初六', isHolidayDay: true, isWork: false},
    // 2022年春节 - 班
    {date: '2022-01-30', holidayName: '春节', isHolidayDay: false, isWork: true},
    {date: '2022-01-29', holidayName: '春节', isHolidayDay: false, isWork: true},
    // 2022年清明节 - 休
    {date: '2022-04-03', holidayName: '清明节', isHolidayDay: false, isWork: false},
    {date: '2022-04-04', holidayName: '寒食节', isHolidayDay: false, isWork: false},
    {date: '2022-04-05', holidayName: '清明节', isHolidayDay: true, isWork: false},
    // 2022年清明节 - 班
    {date: '2022-04-02', holidayName: '清明节', isHolidayDay: false, isWork: true},
    // 2022年劳动节 - 休
    {date: '2022-04-30', holidayName: '劳动节', isHolidayDay: false, isWork: false},
    {date: '2022-05-01', holidayName: '劳动节', isHolidayDay: true, isWork: false},
    {date: '2022-05-02', holidayName: '劳动节', isHolidayDay: false, isWork: false},
    {date: '2022-05-03', holidayName: '劳动节', isHolidayDay: false, isWork: false},
    {date: '2022-05-04', holidayName: '劳动节', isHolidayDay: false, isWork: false},
    // 2022年劳动节 - 班
    {date: '2022-04-24', holidayName: '劳动节', isHolidayDay: false, isWork: true},
    {date: '2022-05-07', holidayName: '劳动节', isHolidayDay: false, isWork: true},
    // 2022年端午节 - 休
    {date: '2022-06-03', holidayName: '端午节', isHolidayDay: true, isWork: false},
    {date: '2022-06-04', holidayName: '端午节', isHolidayDay: false, isWork: false},
    {date: '2022-06-05', holidayName: '端午节', isHolidayDay: false, isWork: false},
    // 2022年中秋节 - 休
    {date: '2022-09-10', holidayName: '中秋节', isHolidayDay: true, isWork: false},
    {date: '2022-09-11', holidayName: '中秋节', isHolidayDay: false, isWork: false},
    {date: '2022-09-12', holidayName: '中秋节', isHolidayDay: false, isWork: false},
    // 2022年国庆 - 休
    {date: '2022-10-01', holidayName: '国庆节', isHolidayDay: true, isWork: false},
    {date: '2022-10-02', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    {date: '2022-10-03', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    {date: '2022-10-04', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    {date: '2022-10-05', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    {date: '2022-10-06', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    {date: '2022-10-07', holidayName: '国庆节', isHolidayDay: false, isWork: false},
    // 2022年国庆 - 班
    {date: '2022-10-08', holidayName: '国庆节', isHolidayDay: false, isWork: true},
    {date: '2022-10-09', holidayName: '国庆节', isHolidayDay: false, isWork: true},
];
